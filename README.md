# Check Google Streetview with Python and GeoJSON

These scripts can be used to check when the most recent Google Streetview images were captured in a certain area.

* testdata.geojson: a geojson file with testdata from the Antwerp (Belgium) region
* almost_empty_template.geojson: a geojson file with just 1 record, that can be used as a template for your own data.
* check.py: The python script that is used to check the Google Streetview API.
* check.bat: A batch file to run multiple checks at a time.


Because a Google API key is personal and linked to a credit card, I replaced my API key by MyApiKey, but if you want to use these scripts, you’ll have to replace that with your own API key.


My blog post about these scripts can be found on https://stuyts.xyz/2020/04/08/how-to-make-a-map-with-the-most-recent-streetview-capture-dates-per-street-with-python-and-geojson/.