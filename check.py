import fiona
import urllib3
import json
import os

from collections import OrderedDict
from operator import getitem
from datetime import date

############################################################
# For all records that have never been checked, the value
# for "lastcheck" should be set to 0 in your GeoJSON file.
# It shouldn't be an empty field.
############################################################

# filename of geojson file
geojsonfile = 'testdata.geojson'

# Google Maps API Key
GoogleMapsKey = "MyApiKey"

# get current date
today = date.today()

# Open geojson file
with fiona.open(geojsonfile, 'r') as input:
    # order testdata: oldest first
    ordered_data = OrderedDict(sorted(
        input.items(),
        key=lambda x: int(getitem(x[1]['properties'], 'lastcheck')),
        reverse=False))

    # get record that hasn't been tested the longest
    oldest_check = list(ordered_data.items())[0][1]

    # Get info on record that has to be checked
    id_to_check = oldest_check['id']
    fid = oldest_check['properties']['fid']
    last_check_date = oldest_check['properties']['lastcheck']
    municipality = oldest_check['properties']['municipality']
    coordinates_to_check = oldest_check['geometry']['coordinates']

    # Create API url
    apiurl = "https://maps.googleapis.com/maps/api/"
    jsonurl = apiurl + "streetview/metadata?output=xml&location=" + \
        str(coordinates_to_check[1]) + \
        "," + \
        str(coordinates_to_check[0]) + \
        "&key=" + GoogleMapsKey

    # Get data from API
    json_response = urllib3.PoolManager().request(
        'GET',
        jsonurl
        ).data.decode('utf-8')

    # Check if Streetview image is available for the checked location
    try:
        data_properties = json.loads(json_response)
        datestreetview = data_properties.get('date')
        location = data_properties.get('location')
        ygoogle = location.get('lat')
        xgoogle = location.get('lng')
    except IndexError:
        ygoogle = "0"
        xgoogle = "0"
        datestreetview = "No images"

    # Print data about checked record
    print(" ")
    print("Number of records in your geojson file: " + str(len(input)))
    print("-------------------------------------------")
    print(" ")
    print("Record to check:")
    print(" FID: " + str(fid))
    print(" Last checked on: " + str(last_check_date))
    print(" Coordinates: ",
          str(coordinates_to_check[0]),
          ", ",
          str(coordinates_to_check[1]))
    print(" Municipality: " + str(municipality))
    print(" ")

    # Print data about found Google Streetview image
    print("Found Streetview data:")
    print(" Date image: " + datestreetview)
    print(" Coordinates: " + str(xgoogle) + ", " + str(ygoogle))

    # create output file
    meta = input.meta
    with fiona.open('output.geojson', 'w', **meta) as output:
        for elem in input:
            if elem['id'] == id_to_check:
                # replace values in checked record
                elem['properties']['lastcheck'] = str(today.strftime("%Y%m%d"))
                elem['properties']['xgoogle'] = str(xgoogle)
                elem['properties']['ygoogle'] = str(ygoogle)
                elem['properties']['datestreetview'] = str(datestreetview)
            # write output file
            output.write({'geometry': elem['geometry'],
                          'id': elem['id'],
                          'properties': elem['properties'],
                          'type': elem['type']})

# Remove input file and rename outputfile to inputfile
os.remove(geojsonfile)
os.rename('output.geojson', geojsonfile)
