@echo off
setlocal ENABLEDELAYEDEXPANSION

REM set the number of records to check
set checks=10
REM ------------------------------

REM set the location of Python 3
SET PYTHONHOME=C:\OSGeo4W64\apps\Python37
REM ------------------------

REM run the checks using 
for /L %%a in (1,1,!checks!) do (
    C:\OSGeo4W64\bin\python3.exe check.py
)
REM --------------------

ENDLOCAL 